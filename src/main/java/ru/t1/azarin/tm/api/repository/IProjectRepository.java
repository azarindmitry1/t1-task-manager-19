package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}