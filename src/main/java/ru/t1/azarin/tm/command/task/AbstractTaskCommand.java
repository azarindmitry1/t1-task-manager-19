package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.api.service.ITaskService;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected List<Task> renderTask(List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        return tasks;
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
