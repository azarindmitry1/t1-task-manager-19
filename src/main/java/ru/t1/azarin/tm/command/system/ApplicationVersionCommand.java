package ru.t1.azarin.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public final static String NAME = "version";

    public final static String ARGUMENT = "-v";

    public final static String DESCRIPTION = "Display application version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.19.0");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
