package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public final static String NAME = "task-list";

    public final static String DESCRIPTION = "Show all tasks.";

    @Override
    public void execute() {
        System.out.println("[ALL TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = serviceLocator.getTaskService().findAll(sort);
        renderTask(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
