package ru.t1.azarin.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public final static String NAME = "task-clear";

    public final static String DESCRIPTION = "Clear all tasks.";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
