package ru.t1.azarin.tm.command.user;

import ru.t1.azarin.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    public final static String NAME = "user-login";

    public final static String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
