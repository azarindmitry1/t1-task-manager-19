package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        user.setRole(role);
        return add(user);
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isLoginExist(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public boolean isEmailExist(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
